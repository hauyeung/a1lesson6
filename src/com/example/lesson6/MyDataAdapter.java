package com.example.lesson6;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MyDataAdapter extends ArrayAdapter<MyData>{

	private LayoutInflater inflater;
	private MyData[] data;
	public MyDataAdapter(Context context, MyData[] data) {
		super(context, R.layout.listitem, data);
        inflater = LayoutInflater.from(context);
        this.data = data;

	}

	public View getView(int position, View convertView, ViewGroup parent)
	{
		 View view = convertView;
		 if (view ==null)
		 {
			 view   = inflater.inflate(R.layout.listitem, null);

		 }
		 
		 MyData item = data[position];
		 if (item != null)
		 {
			 TextView textview = (TextView) view.findViewById(R.id.textview);
			 textview.setText(String.format("%s %s", item.number, item.name));			 
			 
		 }
		return view;
	}	
	
	@Override
	public void notifyDataSetChanged()
	{
		super.notifyDataSetChanged();
	}
}
