package com.example.lesson6;


import java.util.Comparator;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.ToggleButton;

public class MainActivity extends Activity {

	private MyData[] data = new MyData[] {
			  new MyData(10, "Ra"),
			  new MyData(21, "Hathor"),
			  new MyData(8, "Osiris"),
			  new MyData(409, "Sekhmet"),
			  new MyData(12, "Seth"),
			  new MyData(44, "Horus"),
			  new MyData(102, "Isis"),
			  new MyData(33, "Anubis"),
			  new MyData(89, "Apep"),
			  new MyData(71, "Bastet"),
			  new MyData(3, "Thoth"),
			  new MyData(48, "Imhotep")
			};
	ListView listview;
	MyDataAdapter adapter;
	ToggleButton togglebutton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		adapter = new MyDataAdapter(this,data);
		adapter.sort(new Comparator<MyData>() {
	            @Override
	            public int compare(MyData arg0, MyData arg1) {
	                return arg0.name.compareTo(arg1.name);
	            }
	        });
		listview = (ListView) findViewById(R.id.list);
		listview.setAdapter(adapter);		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void sort(View view)
	{		
		boolean on = ((ToggleButton) view).isChecked();
		if (on)
		{
			 adapter.sort(new Comparator<MyData>() {
		            @Override
		            public int compare(MyData arg0, MyData arg1) {
		                return (arg0.name).compareTo(arg1.name);
		            }
		        });
			 adapter.notifyDataSetChanged();	

		}
		else
		{
			 adapter.sort(new Comparator<MyData>() {
		            @Override
		            public int compare(MyData arg0, MyData arg1) {
		                return ((Integer) arg0.number).compareTo((Integer) arg1.number);
		            }
		        });
			 adapter.notifyDataSetChanged();	

		}
	}
	


}
;